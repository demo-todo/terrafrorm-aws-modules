locals {
  root_domain = "${reverse(split(".", var.url))[1]}.${reverse(split(".", var.url))[0]}"
}

data "aws_route53_zone" "selected_zone" {
  name = local.root_domain
}

data "aws_acm_certificate" "issued" {
  provider = aws.us-east-1

  domain   = local.root_domain
  statuses = ["ISSUED"]
}

resource "aws_s3_bucket" "www" {
  bucket        = var.url
  acl           = "private"
  force_destroy = true

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  website {
    index_document = var.root_page
    error_document = var.error_page
  }

  tags = merge(var.tags, map("terraform", true))
}

data "aws_iam_policy_document" "s3-service-policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.www.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    resources = [aws_s3_bucket.www.arn]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "cloudfront-s3-bucket" {
  bucket = aws_s3_bucket.www.id
  policy = data.aws_iam_policy_document.s3-service-policy.json
}

resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "${var.url}-access"
}

resource "aws_cloudfront_distribution" "distribution" {
  origin {
    domain_name = aws_s3_bucket.www.bucket_regional_domain_name
    origin_id   = aws_cloudfront_origin_access_identity.origin_access_identity.id

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  enabled             = true
  comment             = "${var.url} CDN"
  default_root_object = var.root_page
  aliases             = [var.url]

  default_cache_behavior {
    target_origin_id = aws_cloudfront_origin_access_identity.origin_access_identity.id
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    max_ttl                = 86400
    default_ttl            = 3600
    compress               = true

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  custom_error_response {
    error_code            = 404
    error_caching_min_ttl = 0
    response_code         = 200
    response_page_path    = "/${var.error_page}"
  }

  viewer_certificate {
    acm_certificate_arn = data.aws_acm_certificate.issued.arn
    ssl_support_method  = "sni-only"
  }

  tags = merge(var.tags, map("terraform", true))
}

data "aws_route53_zone" "selected" {
  name = local.root_domain
}

resource "aws_route53_record" "alias" {
  zone_id = data.aws_route53_zone.selected.zone_id

  name = var.url
  type = "A"

  alias {
    name                   = aws_cloudfront_distribution.distribution.domain_name
    zone_id                = aws_cloudfront_distribution.distribution.hosted_zone_id
    evaluate_target_health = true
  }
}
