variable "tags" {
  type    = map(any)
  default = {}
}

variable "url" {
  description = "URL of the website"
  type        = string
}

variable "root_page" {
  description = "Root page of the website. Usually Index.html"
  type        = string
  default     = "Index.html"
}

variable "error_page" {
  description = "Error page of the website. Usually Error.html"
  type        = string
  default     = "Error.html"
}
