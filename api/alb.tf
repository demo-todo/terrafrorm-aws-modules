resource "aws_security_group" "alb_sg" {
  name        = "alb-${var.name}-${terraform.workspace}-sg"
  vpc_id      = data.terraform_remote_state.vpc_application.outputs.vpc_id
  description = "Backend Security Group for Aplication Balancer on ${var.name}"

  tags = local.tags
}

resource "aws_security_group_rule" "alb_allow_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb_sg.id
}

resource "aws_security_group_rule" "alb_allow_https" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb_sg.id
}

resource "aws_security_group_rule" "alb_allow_out" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb_sg.id
}

resource "aws_alb" "alb" {
  name            = "${var.name}-${terraform.workspace}-alb"
  internal        = !var.public_access_allow
  security_groups = [aws_security_group.alb_sg.id]
  subnets         = var.public_access_allow ? data.terraform_remote_state.vpc_application.outputs.public_subnets : data.terraform_remote_state.vpc_application.outputs.private_subnets
  idle_timeout    = "120"

  enable_deletion_protection       = var.enable_deletion_protection
  enable_http2                     = true
  enable_cross_zone_load_balancing = var.enable_cross_zone_load_balancing

  tags = local.tags
}

resource "aws_alb_target_group" "service_tg" {
  name     = "${var.name}-${terraform.workspace}-tg"
  port     = 80
  protocol = "HTTP"

  deregistration_delay = var.deregistration_delay
  target_type          = "ip"
  vpc_id               = data.terraform_remote_state.vpc_application.outputs.vpc_id

  health_check {
    interval            = 30
    protocol            = "HTTP"
    port                = 80
    path                = "/"
    healthy_threshold   = 3
    unhealthy_threshold = 5
    timeout             = 5
    matcher             = 200
  }

  depends_on = [aws_alb.alb]

  tags = local.tags
}

resource "aws_alb_listener" "service_https" {
  load_balancer_arn = aws_alb.alb.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"

  certificate_arn = data.terraform_remote_state.acm_certificate.outputs.eu_central_1_arn

  default_action {
    target_group_arn = aws_alb_target_group.service_tg.id
    type             = "forward"
  }
}

resource "aws_alb_listener" "service_http" {
  load_balancer_arn = aws_alb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_route53_record" "ui_alias" {
  zone_id = data.terraform_remote_state.route53.outputs.zone_id
  name    = var.public_url
  type    = "A"

  alias {
    name                   = aws_alb.alb.dns_name
    zone_id                = aws_alb.alb.zone_id
    evaluate_target_health = true
  }

  depends_on = [aws_alb.alb]
}
