data "aws_iam_policy_document" "ecs-task-policy" {
  statement {
    sid    = "ECSTaskExecutionPolicyDevelop"
    effect = "Allow"

    actions = ["ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = ["*"]
  }
}

resource "aws_iam_policy" "ecs-task-policy" {
  name        = "${var.name}-${terraform.workspace}-ecs-task-policy"
  description = "ECS Task Execution Policy"
  path        = "/"
  policy      = data.aws_iam_policy_document.ecs-task-policy.json
}

data "aws_iam_policy_document" "cw-logs-policy" {
  statement {
    sid    = "CloudWatchLogPolicy${terraform.workspace}"
    effect = "Allow"

    actions = ["logs:*"]

    resources = [aws_cloudwatch_log_group.cw-log-group.arn]
  }
}

resource "aws_iam_policy" "cw-logs-policy" {
  name        = "${var.name}-${terraform.workspace}-cw-logs-policy"
  description = "CloudWatch Logs Policy"
  path        = "/"
  policy      = data.aws_iam_policy_document.cw-logs-policy.json
}

data "aws_iam_policy_document" "cloud-watch-log-policy" {
  statement {
    sid     = "CloudWatchLogPolicy${terraform.workspace}"
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      type = "Service"

      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "cloud-watch-log" {
  name               = "cloudwatch-${var.name}-${terraform.workspace}-log"
  description        = "Allow access to CloudWatch logs"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.cloud-watch-log-policy.json
}

data "aws_iam_policy_document" "service-assume-role-policy" {
  statement {
    sid     = "ServiceAssumeRolePolicy"
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      type = "Service"

      identifiers = ["ecs.amazonaws.com",
        "ecs-tasks.amazonaws.com",
      ]
    }
  }
}

resource "aws_iam_role" "service-role" {
  name               = "${var.name}-${terraform.workspace}-role"
  description        = "Allow Service to access ECS and ECS Tasks."
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.service-assume-role-policy.json
}

resource "aws_iam_policy_attachment" "attach-task-policy" {
  name       = "task-attachment"
  roles      = [aws_iam_role.service-role.name]
  policy_arn = aws_iam_policy.ecs-task-policy.arn
}

resource "aws_iam_policy_attachment" "attach-log-policy" {
  name       = "cloudwatchlogs-logs-attachment-${terraform.workspace}"
  roles      = [aws_iam_role.cloud-watch-log.name, aws_iam_role.service-role.name]
  policy_arn = aws_iam_policy.cw-logs-policy.arn
}

data "aws_ecs_task_definition" "service-family" {
  task_definition = aws_ecs_task_definition.service-family.family

  depends_on = [aws_ecs_task_definition.service-family]
}

data "template_file" "container_definition" {
  template = file("../../modules/application/files/service_name-family.json.tpl")

  vars = {
    environment = terraform.workspace
    name        = var.name
    cpu         = "1024"
    memory      = "2048"
  }
}

resource "aws_ecs_task_definition" "service-family" {
  family                   = "${var.name}-${terraform.workspace}-family"
  container_definitions    = data.template_file.container_definition.rendered
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = "1024"
  memory                   = "2048"
  execution_role_arn       = aws_iam_role.service-role.arn
  task_role_arn            = aws_iam_role.cloud-watch-log.arn

  depends_on = [aws_iam_policy_attachment.attach-log-policy]
}


resource "aws_cloudwatch_log_group" "cw-log-group" {
  name = "${var.name}-${terraform.workspace}-group"

  tags = local.tags
}
