data "terraform_remote_state" "vpc_application" {
  backend = "s3"
  config = {
    bucket = "test123-terraform-states"
    region = "eu-central-1"
    key    = "env:/${terraform.workspace}/vpc_application/terraform.tfstate"
  }
}

data "terraform_remote_state" "acm_certificate" {
  backend = "s3"
  config = {
    bucket = "test123-terraform-states"
    region = "eu-central-1"
    key    = "env:/production/acm/terraform.tfstate"
  }
}

data "terraform_remote_state" "ecs" {
  backend = "s3"
  config = {
    bucket = "test123-terraform-states"
    region = "eu-central-1"
    key    = "env:/${terraform.workspace}/ecs/terraform.tfstate"
  }
}

data "terraform_remote_state" "route53" {
  backend = "s3"
  config = {
    bucket = "test123-terraform-states"
    region = "eu-central-1"
    key    = "env:/production/roure54/terraform.tfstate"
  }
}
