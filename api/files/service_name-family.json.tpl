[
    {
      "name": "${name}-${environment}",
      "image": "949161032257.dkr.ecr.eu-central-1.amazonaws.com/${name}:latest",
      "cpu": ${cpu},
      "memory": ${memory},
      "essential": true,
      "portMappings": [
          {
            "hostPort": 80,
            "protocol": "tcp",
            "containerPort": 80
          }
        ],
        "logConfiguration": {
          "logDriver": "awslogs",
          "options": {
            "awslogs-group": "${name}-${environment}-group",
            "awslogs-region": "eu-central-1",
            "awslogs-stream-prefix": "${name}-${environment}"
          }
        }
      }
  ]