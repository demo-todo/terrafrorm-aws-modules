locals {
  tags                     = merge(var.tags, map("terraform", true), map("environment", terraform.workspace))
  create_docker_repository = terraform.workspace == "development" || terraform.workspace == "sandbox" ? true : false
  create_ci_cd             = terraform.workspace == "development" || terraform.workspace == "sandbox" ? true : false
}
