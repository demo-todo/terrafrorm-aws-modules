variable "name" {
  type = string
}

variable "public_url" {
  type    = string
  default = ""
}

variable "public_access_allow" {
  type    = bool
  default = true
}

variable "container_number" {
  default = 1
}

variable "tags" {
  type    = map(any)
  default = {}
}

variable "enable_deletion_protection" {
  type    = bool
  default = false
}

variable "enable_cross_zone_load_balancing" {
  type    = bool
  default = false
}

variable "deregistration_delay" {
  description = "Deregistration Delay"
  default     = "120"
}
