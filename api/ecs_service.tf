# Security group
resource "aws_security_group" "service-container-sg" {
  name   = "${var.name}-${terraform.workspace}-sg"
  vpc_id = data.terraform_remote_state.vpc_application.outputs.vpc_id

  tags = local.tags
}

resource "aws_security_group_rule" "service-container-allow-private" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = var.public_access_allow ? data.terraform_remote_state.vpc_application.outputs.public_subnet_cidr : data.terraform_remote_state.vpc_application.outputs.private_subnet_cidr
  security_group_id = aws_security_group.service-container-sg.id
}

resource "aws_security_group_rule" "be-container-allow-all-out" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.service-container-sg.id
}

# ECS Service
resource "aws_ecs_service" "service-be" {
  name    = "${var.name}-${terraform.workspace}"
  cluster = data.terraform_remote_state.ecs.outputs.this_ecs_cluster_id

  #task_definition                   = "${aws_ecs_task_definition.service-family.family}:${max("${aws_ecs_task_definition.service-family.revision}", "${data.aws_ecs_task_definition.service-family.revision}")}"
  task_definition                   = "${aws_ecs_task_definition.service-family.family}:${max(aws_ecs_task_definition.service-family.revision, data.aws_ecs_task_definition.service-family.revision)}"
  desired_count                     = var.container_number
  launch_type                       = "FARGATE"
  health_check_grace_period_seconds = 120

  load_balancer {
    target_group_arn = aws_alb_target_group.service_tg.arn
    container_name   = "${var.name}-${terraform.workspace}"
    container_port   = 80
  }

  network_configuration {
    subnets         = flatten([data.terraform_remote_state.vpc_application.outputs.private_subnets])
    security_groups = [aws_security_group.service-container-sg.id]
  }
}
