resource "aws_ecr_repository" "repository" {
  count = local.create_docker_repository ? 1 : 0

  name = var.name

  tags = local.tags
}
